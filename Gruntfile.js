//variaveis globais de configuracao
var config = {
  js: 'assets/js',
  css: 'assets/css',
  assets: 'assets',
  app_js: 'assets/app/js',
  app_css: 'assets/app/css',
  app_sass: 'assets/app/sass'
}

module.exports = function(grunt) {
    'use strict';
 
    // configuração do projeto
    var gruntConfig = {
        config: config,
        pkg: grunt.file.readJSON('package.json'),
        bower: grunt.file.readJSON('./.bowerrc'),

        copy: {
            target: {
                files: [{
                  expand: true,
                  cwd: '<%= bower.directory %>/fontawesome',
                  src: 'fonts/*',
                  dest: '<%= config.assets %>'
                }]
            }
        },

        uglify: {
            target: {
                files: {
                    '<%= config.js %>/library.min.js': [
                        '<%= bower.directory %>/jquery/dist/jquery.min.js',
                        '<%= bower.directory %>/jquery-ui/jquery-ui.min.js',
                        '<%= bower.directory %>/bootstrap/dist/js/bootstrap.min.js',
                        '<%= bower.directory %>/angular/angular.min.js'
                    ],
                    '<%= config.js %>/app.min.js':'<%= config.app_js %>/app.js'
                }
            }
        },

        sass: {
            dist: {
                options: {
                    compass: true,
                    style: 'expanded'
                },
                files: {
                    '<%= config.app_css %>/app.css': '<%= config.app_sass %>/app.scss'
                }
            }
        },

        cssmin: {
            target: {
                files: {
                    '<%= config.css %>/library.min.css': [
                        '<%= bower.directory %>/fontawesome/css/font-awesome.min.css',
                        '<%= bower.directory %>/bootstrap/dist/css/bootstrap.min.css'
                    ],
                    '<%= config.css %>/app.min.css':'<%= config.app_css %>/app.css'
                }
            }
        }

    };
 
    grunt.initConfig(gruntConfig);

    // plugins
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-cache-breaker');

    // tarefas
    grunt.registerTask('default', ['copy', 'uglify', 'sass', 'cssmin']);
    //grunt.registerTask('default', ['uglify', 'sass', 'cssmin']);
    //grunt.registerTask('deploy', ['cachebreaker']);
    
    // Tarefa para Watch
    //grunt.registerTask( 'w', [ 'watch' ] );
};