;
(function (window, document, $, undefined) {
    'use strict';

    var app = (function () {

        var $private = {};
        var $public = {};

        $public.showpopover = function () {
            $('button').popover();
        }

        $public.print = function () {
            $('.print').on('click', function (event) {
                event.preventDefault();
                window.print();
            });
        }

        $public.calendar = function() {
            $( "#datepicker" ).datepicker();
        }

        return $public;
    })();

    // Global
    window.app = app;
    app.showpopover();
    app.print();
    app.calendar();

})(window, document, jQuery);